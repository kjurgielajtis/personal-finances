#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_add_new_pushButton_clicked()
{
    bool ok;
        QString text = QInputDialog::getText(this, tr("New wallet"),
                                             tr("Wallet name:"), QLineEdit::Normal,
                                             QDir::home().dirName(), &ok);
        if (ok && !text.isEmpty()) {
            QWidget *wdg = new QWidget(this);
            QRadioButton *wallet_radio_button = new QRadioButton(wdg);
            wallet_radio_button->setText(text);
            //wallet_layout->addWidget(wallet_radio_button);
            //w->setLayout(wallet_layout);
    }
}

void MainWindow::on_pushButton_2_clicked()
{

}
