#include "login_dialog.h"
#include "ui_login_dialog.h"
#include "new_user_dialog.h"
#include "mainwindow.h"
#include <iostream>
#include <string>
#include <fstream>
#include <QFile>
//#include <QByteArray>
//#include <QIODevice>

using namespace std;

login_dialog::login_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::login_dialog)
{
    ui->setupUi(this);

    //Insert already existing users
    /*
    string user;
    ifstream un("data\user_names.txt");
    if(un.is_open()) {
        while(getline(un, user)) {
            ui->user_comboBox->addItem("Jeff");
        }
        un.close();
    }
    */

    QFile file("user_names.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file.atEnd()) {
        QString line = file.readLine();
        ui->user_comboBox->addItem(line);
    }

}

login_dialog::~login_dialog()
{
    delete ui;
}

void login_dialog::on_create_new_user_pushButton_clicked()
{
    new_user_dialog nudialog;
    nudialog.setModal(true);
    nudialog.exec();
}

void login_dialog::on_login_pushButton_clicked()
{
    //TODO: check credentials here
    this->hide();

}
