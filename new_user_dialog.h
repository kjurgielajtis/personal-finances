#ifndef NEW_USER_DIALOG_H
#define NEW_USER_DIALOG_H

#include <QDialog>

namespace Ui {
class new_user_dialog;
}

class new_user_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit new_user_dialog(QWidget *parent = 0);
    ~new_user_dialog();

private:
    Ui::new_user_dialog *ui;
};

#endif // NEW_USER_DIALOG_H
